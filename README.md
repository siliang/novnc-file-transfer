## 介绍
- 实现novnc在web端的文件传输协议
- 可查看远程文件列表（类似ftp）
- 基于TightVnc+novnc
- 设备端（vncserver端）无需改动



## 联系作者
![image](./wechat.jpg)

## 效果预览
![image](./demo.jpg)